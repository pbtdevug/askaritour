/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.util;

import com.midp.lwuit.midlet.AskariTour;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class Update implements Runnable{
    private AskariTour midlet;
    private OutputStream os = null;
    private InputStream is = null,fis = null;
    private HttpConnection conn = null;
    private String userId ="PBT-P41",currentDateTime;
    public String today = AskariTour.getDateTime(),updateSign,pin,upSignature;
    public Vector codes,unitsVector = new Vector();
    public Hashtable guardHashtable = new Hashtable(),locationsHashTable = new Hashtable(),itemsHashtable = new Hashtable();
    public boolean firstUpdate = true;
    
    public Update(AskariTour mid){
        this.midlet = mid;
    }
    
    public void startUpdate(String pin){
        this.pin = pin;
        Thread t = new Thread(this);
        t.start();//starts run() or new thread
    }
    
    private String createUpdateXML(String id,String upSign,String pin){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<updateReq>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<imei>").append(midlet.imei).append("</imei>");//
        xmlStr.append("<iccid>").append(midlet.iccid).append("</iccid>");//
        xmlStr.append("</updateReq>");
        //System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    public void run() {
        String url = AskariTour.connectionUrl;
        System.out.println(url);
        int respCode = 0;
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            //open stream connection
            os = conn.openOutputStream();
            byte [] bytes = createUpdateXML(userId,updateSign,pin).getBytes();
            os.write(bytes, 0, bytes.length);
            
            try{
                respCode = conn.getResponseCode();
                System.out.print(respCode);
            }
            catch(ConnectionNotFoundException ex){
                   
            }
            catch(IOException ex){
                   
            }
            System.out.println("respCode = "+respCode);
            if(respCode == HttpConnection.HTTP_OK){//successful connection to server
                midlet.connectionIsAvailable = true;
                is= conn.openDataInputStream();
                
                int ch ;
                StringBuffer sb = new StringBuffer();
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb.toString());
                
                //viewXML(is);
            }
                
        }
        catch(ParseException ex){
        }
        catch (ConnectionNotFoundException ex){// can't access server
        }
        catch (IOException ex) {
        }
          
        finally{
            if(is!=null){
                try {
                    is.close();
                } 
                catch (IOException ex) {
                }
            }
            if(os!=null){
                try {
                    os.close();
                } 
                catch (IOException ex) {
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } 
                catch (IOException ex) {
                }
            }    
        }
        
        System.out.println("pinForm created");
        midlet.pinForm().show();
    }
    
    private void viewXML(InputStream ips){
        String locaction = new String();
        String action= new String();
        String id = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),
                deploymentOfficerName = new String(),deploymentOfficerID = new String(),
                deploymentOfficerMobileNumber= new String(),officerDets,
                actn = new String(),itemID = new String(),itemName = new String(),itemUnit = new String();
        try{
            InputStreamReader xmlReader = new InputStreamReader(ips);
            XmlParser parser = new XmlParser( xmlReader );
            ParseEvent pe;
            parser.skip();//skips <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, "reply");//start tag <updateResp>
            //
            boolean trucking = true;
            while (trucking) {
            pe = parser.read();
            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                pe = parser.read();
                currentDateTime= pe.getText();
                System.out.println(currentDateTime);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                pe = parser.read();
                upSignature = pe.getText();
                System.out.println(upSignature);
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                String nemu = pe.getName();
                System.out.println("(1) = "+pe.getType());
                System.out.println("(1) = "+nemu);
                if (nemu.equals("update")) {
                    while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                        parser.skip();
                        pe = parser.peek();
                        System.out.println("(2) = "+pe.getType());
                        System.out.println("(2) = "+pe.getName());
                        if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                            String name1 = pe.getName();
                            if (name1.equals("record")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                        pe = parser.read();
                                        gName = pe.getText();
                                        //name = gName;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                        pe = parser.read();
                                        number = pe.getText();
                                        //phoneNum = number;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                        pe = parser.read();
                                        id = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                       pe = parser.read();
                                        action = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                        pe = parser.read();
                                        locaction = pe.getText();
                                    }
                                }
                                //hashtable for guards
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(number==null||locaction==null){
                                        number = "null";
                                        locaction = "null";
                                    }
                                    guardHashtable.put(id.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    guardHashtable.remove(id.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                    pe = parser.read();
                            }
                        }
                        else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("deployment")){
                            System.out.println("(3) = "+pe.getType());
                            System.out.println("(3) = "+pe.getName());
                            String nam = pe.getName();
                            if (nam.equals("deployment")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentName")) {
                                        pe = parser.read();
                                        deploymentName = pe.getText();
                                        System.out.println(deploymentName);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerName")) {
                                        pe = parser.read();
                                        deploymentOfficerName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerMobileNumber")) {
                                        pe = parser.read();
                                        deploymentOfficerMobileNumber = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerID")) {
                                        pe = parser.read();
                                        deploymentOfficerID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                        //System.out.println(action);
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentID")) {
                                        pe = parser.read();
                                        deploymentID = pe.getText();
                                    }
                                }
                                //hashtable for locations
                                officerDets = deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+deploymentName.trim();
                                if("INSERT".equalsIgnoreCase(action)){
                                    locationsHashTable.put(deploymentID.trim(), officerDets);
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    locationsHashTable.remove(deploymentID.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                    pe = parser.read();
                            }
                        }
                        else if (pe.getType() == Xml.START_TAG&&pe.getName().equals("item")){
                            String atbNemu = pe.getName();
                            if (atbNemu.equals("item")) {
                                while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(atbNemu) == false)){
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemID")) {
                                        pe = parser.read();
                                        itemID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemName")) {
                                        pe = parser.read();
                                        itemName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemUnit")) {
                                        pe = parser.read();
                                        itemUnit = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                    }
                                }
                                //hashtable for items
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(itemID==null||itemName==null||itemUnit==null){
                                        itemUnit = "null";
                                        itemName = "null";
                                        itemID = "null";
                                    }
                                    unitsVector.addElement(itemUnit);//avoid duplicates
                                    itemsHashtable.put(itemID.trim(), itemName.trim()+"_"+itemUnit.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    itemsHashtable.remove(itemID.trim());
                                }
                            }
                            else{
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(atbNemu) == false))
                                    pe = parser.read();
                            }
                        }
                    }
                }
                else {
                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                    pe = parser.read();
                }
            }
            if (pe.getType() == Xml.END_TAG &&pe.getName().equals("reply"))
                trucking = false;
            }
            System.out.println("Guards Hashtable: "+guardHashtable.toString());
            System.out.println("Locations Hashtable: "+locationsHashTable.toString());
            System.out.println("Items Hashtable: "+itemsHashtable.toString());
            today = AskariTour.split(currentDateTime, " ").elementAt(0).toString();
            RecordStore.deleteRecordStore("guardRecords");//remove previous records
            byte [] byts = (upSignature+"*"+guardHashtable.toString()+"*"+locationsHashTable.toString()+"*"+itemsHashtable.toString()).getBytes();//get byte representation
            RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
            rst.addRecord(byts, 0, byts.length);
            rst.closeRecordStore();//save update guard records
            midlet.id = pin;
            //Vector guardDetails = IGMS.split(guardHashtable.get(pin).toString(),"_");//split returned string by _
            //midlet.name = guardDetails.elementAt(0).toString();
            //System.out.println("main Menu");
            //midlet.switchDisplayable(null, midlet.mainMenu());
            /*if(firstUpdate == true){
                midlet.login();
                firstUpdate = false;
            }*/
        }
        catch (Exception ex) { 
            //ex.printStackTrace();
        }
    }
}
