/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.midlet;

import com.midp.lwuit.util.ConnectToServer;
import com.midp.lwuit.util.Update;
import com.sun.lwuit.Command;
import com.sun.lwuit.Component;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.Display;
import com.sun.lwuit.Form;
import com.sun.lwuit.Image;
import com.sun.lwuit.Label;
import com.sun.lwuit.Painter;
import com.sun.lwuit.Slider;
import com.sun.lwuit.TextArea;
import com.sun.lwuit.TextField;
import com.sun.lwuit.animations.Transition;
import com.sun.lwuit.events.ActionEvent;
import com.sun.lwuit.events.ActionListener;
import com.sun.lwuit.events.FocusListener;
import com.sun.lwuit.geom.Dimension;
import com.sun.lwuit.layouts.BorderLayout;
import com.sun.lwuit.plaf.Border;
import com.sun.lwuit.plaf.UIManager;
import com.sun.lwuit.table.TableLayout;
import com.sun.lwuit.util.Resources;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.midlet.*;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
//import net.sf.microlog.core.Logger;
//import net.sf.microlog.core.LoggerFactory;
//import net.sf.microlog.core.PropertyConfigurator;


/**
 * @author hudson Bogere
 */
public class AskariTour extends MIDlet implements FocusListener  {
    private Painter diagonalPainter;
    private TextField f,f1;
    private Form splashScreen,pinForm,progressForm,waitForm;
    public Form state,checkPtForm,configForm;
    private Image imgSplash,progressImage;
    public Image alarmImage,errorImage,successImage,imgIcon;
    private TableLayout tableLayout,tableLayout2;
    private Resources r;
    private TextArea dialogMsgLabel;
    private Label imageLabel,pbtLabel,idLabel,iconLabel,waitLabel,otpLabel,dialogLabel,iccidLabel,imeiLabel,urlLabel;
    private TextField otpTextField,idTextField,pinTextArea,imeiTextField,iccidTextField,urlTextField;
    private Command submitCommand,cancelCommand,exitCommand,okCommand,clearCommand;
    public String pin,otp,id,imei,url,iccid;
    public StringBuffer activeTrxn = new StringBuffer();
    public Update up = new Update(this);
    private ConnectToServer cts = new ConnectToServer(this);
    public boolean connectionIsAvailable = false,historyFlag = false,pendingSendFlag =false,hisPendFlag = false;
    private Slider slider;
    private Dialog dialog;
    private RecordStore rst;
    private int numRecords = 0;
    public String Version = "Tue July 08 20:52:56 UTC 2014";
    
    private Component activeTextField;
    
    //public static String connectionUrl = "http://162.144.57.3/cgi-bin/askari/mobile.redirect";
    public static String connectionUrl = "http://www.askariplus.com/askari/backend/mobile/mobile.php";
    
    private int screenwidth,screenHeight;
    private boolean midletPaused = false;
    // A logger instance for this class
    //private static final Logger log = LoggerFactory.getLogger(AskariTour.class);

//<editor-fold defaultstate="collapsed" desc="AskariTour">
    public AskariTour(){
        String config;
        System.out.println("constractor inoked");
        try{
            rst = RecordStore.openRecordStore("config", true);
            numRecords = rst.getNumRecords();
            if(numRecords>0){
                RecordEnumeration re = rst.enumerateRecords(null, null, false);
                byte [] byts = null;
                while(re.hasNextElement()){
                    byts = re.nextRecord();
                    break;
                }
                config = new String(byts,0,byts.length);
                
                System.out.println("config String = "+config);
                
                Vector contents = split(config,"*");
                
                imei = contents.elementAt(0).toString();
                System.out.println(imei);
                iccid = contents.elementAt(1).toString();
                System.out.println(iccid);
                url = contents.elementAt(2).toString();
                connectionUrl = url;
                System.out.println(url);
            }
            rst.closeRecordStore();
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="startApp">
    public void startApp() {
        //log.info("Starting application");
        //init the LWUIT Display
        System.out.println("App started");
        if(midletPaused){
            resumeMIDlet();
        }
        else{
            Display dis = Display.getInstance();
            Display.init(this);
        
            screenwidth = dis.getDisplayWidth();
            screenHeight = dis.getDisplayHeight();
        
            try {
                r = Resources.open("/splahresourceFile.res");
                alarmImage = r.getImage("alarm");
                errorImage = r.getImage("error");
                successImage = r.getImage("success");
                imgIcon = r.getImage("imgIcon");
                //logo = r.getImage("logo");
                UIManager.getInstance().setThemeProps(r.getTheme("splashTheme"));
                getSplashScreen();
            } 
            catch (java.io.IOException e) {
                e.printStackTrace();
            }         
        }
        midletPaused = false;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="pauseApp">    
    public void pauseApp() {
        //log.info("Pausing application");
        midletPaused = true;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="destroyApp">    
    public void destroyApp(boolean unconditional) {
        //log.info("Destroying application");
        // Shutdown Microlog gracefully
        //LoggerFactory.shutdown();
         //notifyDestroyed();
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    private void  getSplashScreen() throws IOException {
        //log.debug("splashScreen");
        System.out.println("splash created");
        splashScreen = new Form();
        splashScreen.setLayout(new BorderLayout());
        splashScreen.setTitle("Guard Patrol");
        //splashScreen.addComponent(imageLabel());
        splashScreen.addComponent(BorderLayout.CENTER,pbtLabel());
        
        long startTime = System.currentTimeMillis();
        if(numRecords>0){
            splashScreen.show();
            int count = 0;
            //delay
            while(System.currentTimeMillis()-startTime <= 3000){
              count++;
              //System.out.println(count);
            }
            
            waitForm().show();
            up.startUpdate(pin);
        }
        else{
            configForm().show();
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imgSplash">
    private Image imgSplash(){
        if(imgSplash == null){
            imgSplash =  r.getImage("splashScreen");
        }
        return imgSplash;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imgSplash">
    /*private Image imgIcon(){
        if(imgIcon == null){
            try{
                imgIcon = r.getImage("splashScreen");
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        return imgIcon;
    }*/
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" progressImage ">
    private Image progressImage(){
        if(progressImage == null){
            progressImage =  r.getImage("sandglass");
        }
        return progressImage;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="pinForm">    
    public Form pinForm(){
        if(pinForm == null){
            pinForm = new Form();
            pinForm.setTitle("Enter Your 4 Digit PIN");
            pinForm.setLayout(new BorderLayout());
            pinForm.addComponent(BorderLayout.CENTER,pinTextArea());
            //pinForm.addCommand(okCommand());
            pinForm.addCommand(exitCommand());
            pinForm.addCommand(clearCommand());
            pinForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    //if(evt.getSource() == Canvas.FIRE){
                        pin = pinTextArea.getText();
                        pinTextArea.setText("");
                        if(pin.length()!=4){
                            //log.debug("PIN length != 4");
                            //Dialog.show("Exception", e.getMessage(), "OK", null);
                            //Dialog.setAutoAdjustDialogSize(true);
                            //Dialog.
                            dialog("PIN Error", "PIN must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            //Dialog.show("PIN Error", new Label("PIN must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                        }
                        else{
                            //log.debug("Switch to CheckPtForm");
                            checkPtForm().show();
                        }
                    //}
                }
            });
            pinForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == clearCommand){
                        String content = pinTextArea.getText();
                        int length = content.length();
                        if(length>0){
                            content = content.substring(0, length-1);
                            pinTextArea.setText(content);
                        }
                    }/*else if(evt.getCommand() == okCommand){
                        //log.debug("User pressed the okCommand command.");
                        pin = pinTextArea.getText();
                        pinTextArea.setText("");
                        if(pin.length()!=4){
                            //Dialog.show("Exception", e.getMessage(), "OK", null);
                            //log.debug("PIN length != 4");
                            //Dialog.setAutoAdjustDialogSize(true);
                            dialog("PIN Error", "PIN must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            //Dialog.show("PIN Error", new Label("PIN must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                        }
                        else{
                            //log.debug("Switch to CheckPtForm");
                            checkPtForm().show();
                        }
                    }*/
                    else{
                        //log.debug("Exit Command Selected");
                        exitMIDlet();
                    }
                }
            });
        }
        System.out.println("pinForm showing");
        return pinForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="checkPtForm">    
    public Form checkPtForm(){
        if(checkPtForm == null){
            checkPtForm = new Form();
            checkPtForm.setTitle("Guard Patrol");
            checkPtForm.setSize(new Dimension(screenwidth,screenHeight));
            checkPtForm.setScrollable(true);
            checkPtForm.setScrollVisible(true);
            checkPtForm.setLayout(tableLayout());
            TableLayout.Constraint constraint1 = tableLayout().createConstraint(1,0);
            constraint1.setHorizontalAlign(Component.CENTER);
            constraint1.setHeightPercentage(12);
            checkPtForm.addComponent(constraint1,otpLabel());
            constraint1 = tableLayout.createConstraint(2,0);
            //constraint.setHeightPercentage(14);
            constraint1.setWidthPercentage(100);
            checkPtForm.addComponent(constraint1,otpTextField());//5748
            constraint1 = tableLayout.createConstraint(3,0);
            constraint1.setHorizontalAlign(Component.CENTER);
            constraint1.setHeightPercentage(12);
            checkPtForm.addComponent(constraint1,idLabel());
            constraint1 = tableLayout.createConstraint(4,0);
            //constraint.setHeightPercentage(14);
            constraint1.setWidthPercentage(100);
            checkPtForm.addComponent(constraint1,idTextField());
            constraint1 = tableLayout.createConstraint(5,0);
            constraint1.setHorizontalAlign(Component.CENTER);
            constraint1.setHeightPercentage(50);
            checkPtForm.addComponent(constraint1,iconLabel());
            //checkPtForm.addCommand(submitCommand());
            checkPtForm.addCommand(cancelCommand());
            checkPtForm.addCommand(clearCommand());
            checkPtForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    otp = otpTextField.getText();
                    id = idTextField.getText();
                    idTextField.setText("");
                    otpTextField.setText("");
                    if(otp.length()!=6){
                        //Dialog.show(status, new Label(status), null, Dialog.TYPE_INFO, midlet.successImage, 5000,getTransition());
                        //Dialog.setAutoAdjustDialogSize(true);
                        dialog("OTP Error", "OTP must be 6 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                        //Dialog.show("OTP Error", new Label("OTP must be 6 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                    }
                    else{
                        if(id.length()!=4){
                            //Dialog.setAutoAdjustDialogSize(true);
                            dialog("ID Error", "ID must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            //Dialog.show("CheckPtID Error", new Label("CheckPtID must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                        }
                        else{
                            state = checkPtForm;
                            //String opt,String chkptID,String action,String desc, String pin,String transNum
                            cts.setTransaction(otp, id, "", "", pin, getTrxnNumber());
                            cts.startConnection();
                            waitForm().setTitle("Submitting...");
                            //waitLabel().setText("Connecting....");
                            waitForm().showBack();
                        }
                    }
                }
            });
            checkPtForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(evt.getCommand() == submitCommand){
                        //log.debug("Submit Command Selected");
                        otp = otpTextField.getText();
                        id = idTextField.getText();
                        idTextField.setText("");
                        otpTextField.setText("");
                        if(otp.length()!=6){
                            //log.debug("OTP length != 6");
                            //Dialog.show(status, new Label(status), null, Dialog.TYPE_INFO, midlet.successImage, 5000,getTransition());
                            //Dialog.setCommandsAsButtons(true);
                            dialog("OTP Error", "OTP must be 6 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            //Dialog.show("OTP Error", new Label("OTP must be 6 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                        }
                        else{
                            if(id.length()!=4){
                                //log.debug("PIN length != 4");
                                //Dialog.setCommandsAsButtons(true);
                                dialog("ID Error", "ID must be 4 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                                //Dialog.show("OTP Error", new Label("CheckPtID must be 4 digits long"), null, Dialog.TYPE_ERROR, alarmImage, 5000,cts.getTransition());
                            }
                            else{
                                //log.debug("Submitting Transaction ....");
                                state = checkPtForm;
                                //String opt,String chkptID,String action,String desc, String pin,String transNum
                                cts.setTransaction(otp, id, "", "", pin, getTrxnNumber());
                                cts.startConnection();
                                waitForm().setTitle("Submitting...");
                                //waitLabel().setText("Connecting....");
                                waitForm().showBack();
                            }
                        }
                    }
                    else if(evt.getCommand() == clearCommand){
                        TextField textField = ((TextField)activeTextField);
                        String content = textField.getText();
                        int length = content.length();
                        if(length>0){
                            content = content.substring(0, length-1);
                            textField.setText(content);
                        }
                    }
                    else{
                        //log.debug("Back or Cancel Command Selected");
                        pinForm().showBack();
                    }
                }
            }); 
        }
        return checkPtForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="pinTextArea">    
    private TextField pinTextArea(){
        if(pinTextArea == null){
            pinTextArea = new TextField();
            pinTextArea.setVisible(true);
            pinTextArea.setEditable(true);
            pinTextArea.setEnabled(true);
            pinTextArea.setColumns(20);
            pinTextArea.setInputMode("123");
            //pinTextArea.setReplaceMenu(false);
            pinTextArea.setUseSoftkeys(false);
            pinTextArea.setMaxSize(4);
            //pinTextArea.setLabelForComponent(new Label("Enter Your Digit PIN"));
            pinTextArea.getStyle().setBgTransparency(100);
            pinTextArea.addFocusListener(this);
        }
        return pinTextArea;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="otpTextField">    
    private TextField otpTextField(){
        if(otpTextField == null){
            otpTextField = new TextField(TextField.NUMERIC);
            otpTextField.setVisible(true);
            otpTextField.setEditable(true);
            otpTextField.setEnabled(true);
            otpTextField.setHint("OTP");
            //otpTextField.setReplaceMenu(false);
            otpTextField.setUseSoftkeys(false);
            otpTextField.setColumns(6);
            otpTextField.setMaxSize(6);
            //otpTextField.setLabelForComponent(otpLabel());
            otpTextField.setInputMode("123");
            otpTextField.getStyle().setBgTransparency(100);
            otpTextField.addFocusListener(this);
        }
        return otpTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="idTextField">    
    private TextField idTextField(){
        if(idTextField == null){
            idTextField = new TextField(TextField.NUMERIC);
            idTextField.setVisible(true);
            idTextField.setEditable(true);
            idTextField.setEnabled(true);
            idTextField.setHint("Check Point ID");
            idTextField.setInputMode("123");
            //idTextField.setReplaceMenu(false);
            idTextField.setUseSoftkeys(false);
            idTextField.setMaxSize(4);
            //idTextField.setLabelForComponent(idLabel());
            idTextField.getStyle().setBgTransparency(100);
            idTextField.setColumns(6);
            idTextField.addFocusListener(this);
        }
        return idTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imeiTextField">    
    private TextField imeiTextField(){
        if(imeiTextField == null){
            imeiTextField = new TextField(TextField.NUMERIC);
            imeiTextField.setVisible(true);
            imeiTextField.setEditable(true);
            imeiTextField.setEnabled(true);
            imeiTextField.setHint("IMEI");
            imeiTextField.setInputMode("123");
            imeiTextField.setMaxSize(15);
            imeiTextField.setReplaceMenu(false);
            imeiTextField.setLabelForComponent(idLabel());
            imeiTextField.getStyle().setBgTransparency(100);
            imeiTextField.setColumns(6);
        }
        return imeiTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="iccidTextField">    
    private TextField iccidTextField(){
        if(iccidTextField == null){
            iccidTextField = new TextField(TextField.NUMERIC);
            iccidTextField.setVisible(true);
            iccidTextField.setEditable(true);
            iccidTextField.setEnabled(true);
            iccidTextField.setHint("ICCID");
            iccidTextField.setInputMode("123");
            iccidTextField.setMaxSize(19);
            iccidTextField.setReplaceMenu(false);
            iccidTextField.setLabelForComponent(idLabel());
            iccidTextField.getStyle().setBgTransparency(100);
            iccidTextField.setColumns(6);
        }
        return iccidTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="urlTextField">    
    private TextField urlTextField(){
        if(urlTextField == null){
            urlTextField = new TextField(TextField.NUMERIC);
            urlTextField.setText("http://www.askariplus.com/askari/backend/mobile/mobile.php");
            urlTextField.setVisible(true);
            urlTextField.setEditable(true);
            urlTextField.setEnabled(true);
            urlTextField.setHint("URL");
            urlTextField.setMaxSize(150);
            urlTextField.setInputMode("abc");
            urlTextField.setEnableInputScroll(true);
            urlTextField.setLabelForComponent(idLabel());
            urlTextField.getStyle().setBgTransparency(100);
            urlTextField.setColumns(6);
        }
        return urlTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="idLabel()">
    private Label idLabel(){
        if(idLabel == null){
            idLabel = new Label("Check Point ID");
            idLabel.getStyle().setFgColor(0xffffff);
            idLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return idLabel; 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imeiLabel">
    private Label imeiLabel(){
        if(imeiLabel == null){
            imeiLabel = new Label("Enter Device IMEI");
            imeiLabel.getStyle().setFgColor(0xffffff);
            imeiLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return imeiLabel; 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="iccidLabel">
    private Label iccidLabel(){
        if(iccidLabel == null){
            iccidLabel = new Label("Enter SIM ICCID");
            iccidLabel.getStyle().setFgColor(0xffffff);
            iccidLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return iccidLabel; 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="urlLabel">
    private Label urlLabel(){
        if(urlLabel == null){
            urlLabel = new Label("Enter Connection URL");
            urlLabel.getStyle().setFgColor(0xffffff);
            urlLabel.getStyle().setBgTransparency(0);
            //idLabel.setSize(new Dimension(4,6));
        }
       return urlLabel; 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="imageLabel">
    private Label imageLabel(){
        if(imageLabel == null){
            imageLabel = new Label(imgSplash());
            //imageLabel.getStyle().setFgColor(0xffffff);
            imageLabel.getStyle().setBorder(Border.createLineBorder(2));
            imageLabel.setAlignment(Component.CENTER);
        }
       return imageLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="iconLabel">
    private Label iconLabel(){
        if(iconLabel == null){
            iconLabel = new Label(imgIcon);
            iconLabel.getStyle().setBgTransparency(0);
            //iconLabel.getStyle().setBorder(Border.createLineBorder(2));
            iconLabel.setAlignment(Component.CENTER);
        }
       return iconLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="dialogLabel">
    private Label dialogLabel(Image img){
        //if(dialogLabel == null){
            dialogLabel = new Label(img);
            //imageLabel.getStyle().setFgColor(0xffffff);
            //dialogLabel.getStyle().setBorder(Border.createLineBorder(2));
            dialogLabel.setAlignment(Component.CENTER);
        //}
       return dialogLabel; 
    }
    
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc="otpLabel">
    private Label otpLabel(){
        if(otpLabel == null){
            otpLabel = new Label("OTP");
            otpLabel.getStyle().setBgTransparency(0);
            otpLabel.getStyle().setFgColor(0xffffff);
            //otpLabel.setShiftText(screenwidth);
            //otpLabel.setSize(new Dimension(4,6));
        }
       return otpLabel; 
    }
    
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc="waitLabel">
    private Label waitLabel(){
        if(waitLabel == null){
            waitLabel = new Label("Connecting ....");
            //otpLabel.getStyle().set3DText(true, true);
            waitLabel.getStyle().setBgTransparency(0);
            waitLabel.getStyle().setFgColor(0xffffff);
        }
       return waitLabel; 
    }
    
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="pbtLabel">
    private Label pbtLabel(){
        if(pbtLabel == null){
            pbtLabel = new Label(imgSplash());
            pbtLabel.setAlignment(Component.CENTER);
            pbtLabel.getStyle().setBgTransparency(0);
            pbtLabel.setText("Powered by PBT");
            pbtLabel.setTextPosition(Component.BOTTOM);
            pbtLabel.getStyle().setFgColor(0xffffff);

        }
       return pbtLabel; 
    }
    
//</editor-fold>     
   
//<editor-fold defaultstate="collapsed" desc=" cancelCommand ">
    private Command cancelCommand() {
        if (cancelCommand == null) {
            cancelCommand = new Command("Cancel", 1);
        }
        return cancelCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="exitCommand">
    private Command exitCommand() {
        if (exitCommand == null) {
            exitCommand = new Command("Exit", 1);
        }
        return exitCommand;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" submitCommand ">
    private Command submitCommand() {
        if (submitCommand == null) {
            submitCommand = new Command("Submit", 0);
        }
        return submitCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" okCommand ">
    private Command okCommand() {
        if (okCommand == null) {
           okCommand = new Command("Ok", 0);
        }
        return okCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="clearCommand">
    private Command clearCommand() {
        if (clearCommand == null) {
           clearCommand = new Command("Clear", 1);
        }
        return clearCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" exitMIDlet">
    public void exitMIDlet() {
        destroyApp(true);
        notifyDestroyed();
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="resumeMIDlet">
    public void resumeMIDlet() {
        pinForm().show();
    }
//</editor-fold>    
  
//<editor-fold defaultstate="collapsed" desc="configForm">    
    private Form configForm(){
        if(configForm == null){ 
            configForm = new Form("Configurations");
            configForm.setSize(new Dimension(screenwidth,screenHeight));
            configForm.setScrollable(true);
            configForm.setScrollVisible(true);
            configForm.setLayout(tableLayout2());
            TableLayout.Constraint constraint = tableLayout2.createConstraint(6,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,imeiLabel());
            constraint = tableLayout2.createConstraint(7,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,imeiTextField());
            constraint = tableLayout2.createConstraint(8,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,iccidLabel());
            constraint = tableLayout2.createConstraint(9,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,iccidTextField());
            constraint = tableLayout2.createConstraint(10,0);
            constraint.setHorizontalAlign(Component.LEFT);
            constraint.setHeightPercentage(12);
            configForm.addComponent(constraint,urlLabel());
            constraint = tableLayout2.createConstraint(11,0);
            constraint.setWidthPercentage(100);
            configForm.addComponent(constraint,urlTextField());
            configForm.addGameKeyListener(Canvas.FIRE, new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    imei = imeiTextField.getText();
                    System.out.println(imei);
                    iccid = iccidTextField.getText();
                    System.out.println(iccid);
                    url = urlTextField.getText();
                    System.out.println(url);
                    connectionUrl = url;
                    
                    imeiTextField.setText("");
                    iccidTextField.setText("");
                    urlTextField.setText("");
                    
                    if(imei.length()==15){
                        if(iccid.length()==19){
                            if(url.length()>20){
                                String config = imei+"*"+iccid+"*"+url;
                                byte[] bytes = config.getBytes();
                                try{
                                    RecordStore rstr = RecordStore.openRecordStore("config", true);
                                    rstr.addRecord(bytes, 0, bytes.length);
                                    rstr.closeRecordStore();
                                    
                                    System.out.println("config String = "+config);
                                    
                                     waitForm().show();
                                     up.startUpdate(pin);
                                     
                                }
                                catch(RecordStoreException ex){
                                    ex.printStackTrace();
                                }
                                
                            }
                            else{
                                //Dialog.show("URL Error", "Invalid URL", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                                dialog("URL Error", "Invalid URL", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                            }
                        }
                        else{
                           dialog("ICCID Error", "ICCID must be 19 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000); 
                           //Dialog.show("ICCID Error", "ICCID must be 19 digits", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                        }
                    }
                    else{
                        dialog("IMEI Error", "IMEI must be 15 digits", cts.getTransition(),Dialog.TYPE_ERROR,alarmImage,5000);
                        //Dialog.show("IMEI Error", "IMEI must be 15 digits", null, Dialog.TYPE_ERROR, alarmImage, 50000);
                    }
                }
            });
        }
        return configForm;
    } 
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="waitForm">    
    private Form waitForm(){
        if(waitForm == null){ 
            waitForm = new Form("Checking for updates");
            waitForm.setLayout(new BorderLayout());
            waitForm.addComponent(BorderLayout.SOUTH,slider());
            waitForm.addComponent(BorderLayout.CENTER,waitLabel());
            waitForm.addCommand(cancelCommand());
            waitForm.addCommandListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if(state!=null){
                        checkPtForm.show();
                    }
                    else{
                        pinForm().show();
                    }
                }
            });
        }
        System.out.println("waitForm showing");
        return waitForm;
    } 
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="tableLayout">    
    private TableLayout tableLayout(){
        if(tableLayout == null){ 
            tableLayout = new TableLayout(5,1);
            tableLayout.getPreferredSize(checkPtForm);
        }
        return tableLayout;
    } 
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc="tableLayout2">    
    private TableLayout tableLayout2(){
        if(tableLayout2 == null){ 
            tableLayout2 = new TableLayout(6,1);
            tableLayout2.getPreferredSize(configForm);
        }
        return tableLayout2;
    } 
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc="dialog">  
    //show("Error", new Label(status), null, Dialog.TYPE_INFO, null, 5000,getTransition());
    public void dialog(String title,String msg,Transition trans,int type,Image icon,int timeOut){
        //System.out.println("Dialog");
        //state.show();
        //if(dialog == null){ 
            //System.out.println("Dialog1");
            dialog = new Dialog();
            //dialog.setSize(new Dimension(screenwidth/2,screenHeight/2));
            //dialog.getStyle().setBgTransparency(100);
            dialog.getStyle().setBgColor(timeOut);
            dialog.getStyle().setBackgroundGradientEndColor(0xfb060e);
            dialog.setLayout(new BorderLayout());
            dialog.setScrollable(true);
            dialog.setTitle(msg);
            //dialog.addComponent(BorderLayout.CENTER,new Label(msg));
            dialog.addComponent(BorderLayout.NORTH,dialogLabel(icon));
            //System.out.println("Dialog5");
            dialog.flushReplace();
            //System.out.println("Dialog6");
            dialog.setDialogType(type);
            dialog.setTransitionOutAnimator(trans);
            dialog.setTransitionInAnimator(trans);
            dialog.setTimeout(timeOut);
            //System.out.println("Dialog7");
        //}
        //dialog.show();    
        dialog.showPacked("Center", true);
    } 
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc="slider">    
    private Slider slider(){
        if(slider == null){ 
            slider = new Slider();
            slider.setInfinite(true);
            slider.animate();
            slider.getStyle().setBgTransparency(100);
            slider.setText("please wait");
            slider.setSmoothScrolling(true);
            slider.getStyle().setFgColor(0xffffff);
        }
        return slider;
    } 
//</editor-fold>
          
//<editor-fold defaultstate="collapsed" desc="split">
    public static Vector split(String splitStr, String delimiter) {
        //String [] splitArray;
            StringBuffer token = new StringBuffer();
            Vector tokens = new Vector();

            // split
            char[] chars = splitStr.toCharArray();
            for (int i=0; i < chars.length; i++) {
                if (delimiter.indexOf(chars[i]) != -1) {
                    // we bumbed into a delimiter
                    if (token.length() > 0) {
                        tokens.addElement(token.toString());
                        token.setLength(0);
                    }
                }
                else {
                    token.append(chars[i]);
                }
            }
            // don't forget the "tail"...
            if (token.length() > 0) {
                tokens.addElement(token.toString());
            }
            return tokens;
        }
    //</editor-fold>    
  
//<editor-fold defaultstate="collapsed" desc=" getDateTime">    
    public static String getDateTime() {//Fri 29 Nov 2013
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(5).toString()+"-"+mnt+"-"+dateInfo.elementAt(2);
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getTrxnNumber">    
    public String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="focusGained">
    public void focusGained(Component cmpnt) {
        activeTextField = cmpnt;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="focusLost">
    public void focusLost(Component cmpnt) {
        
    }
//</editor-fold>
    
}
