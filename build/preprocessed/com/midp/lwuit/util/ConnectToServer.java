/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.midp.lwuit.util;

import com.midp.lwuit.midlet.AskariTour;
import com.sun.lwuit.Dialog;
import com.sun.lwuit.animations.CommonTransitions;
import com.sun.lwuit.animations.Transition;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class ConnectToServer implements Runnable {
    private AskariTour midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private HttpConnection conn = null;
    private Transition trans;
    private int expShifts;
    public String status;
    private String txNum,id="",shift,pin,location="",name="",confNum="",
            currentDateTime,today,replyRootTag,upSignature,locationID,adj,opt,actn,chkPtID,
            itemId,itemQty,itemUniti,descrip,phoneNum,itemNemu;
    
    public ConnectToServer(AskariTour midlet){
        this.midlet = midlet;
        today = midlet.up.today;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        status = null;
        th.start();
    }
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String locationID,String trxnNum){
        this.txNum = trxnNum;
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.locationID = locationID;
        this.location = location;
    }
    
    public void setTransaction(String location,String locationID,int expShifts,String pin,String transNum,String adj){
        this.expShifts = expShifts;
        this.location = location;
        this.pin = pin;
        this.txNum = transNum;
        this.locationID = locationID;
        this.adj = adj;
    }
    
    public void setTransaction(String opt,String chkptID,String action,String desc, String pin,String transNum){
        this.opt = opt;
        this.chkPtID = chkptID;
        this.pin = pin;
        this.descrip = desc;
        this.txNum = transNum;
        this.actn = action;
    }
    
    public void setTransaction(String location,String locationID,String itemID,String itemName,String itemUnit,String itemQty, String pin,String transNum){
        this.location = location;
        this.locationID = locationID;
        this.itemId = itemID;
        this.itemNemu = itemName;
        this.itemUniti = itemUnit;
        this.itemQty = itemQty;
        this.pin = pin;
        this.txNum = transNum;
    }
    
    public void run() {
        //String url = "http://localhost:8080/MarcsWeb/Tolls";
        String url = AskariTour.connectionUrl;
        byte [] bytes;
        int respCode =0;
        replyRootTag = "reply";
        if(midlet.state == midlet.checkPtForm){
            bytes = createTransactionXML(opt,chkPtID,actn,pin,txNum).getBytes();
        }
        else{
            bytes = createTransactionXML(name,id,shift,pin,location,txNum).getBytes();
        }
        //while(status == null){
            try {
                conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
                conn.setRequestMethod(HttpConnection.POST);
                conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
                conn.setRequestProperty("Content-Language", "en-CA");
                conn.setRequestProperty("Content-Type","text/xml");
                os = conn.openOutputStream();
                os.write(bytes, 0, bytes.length);//write transaction to outputstream
                try{
                    respCode = conn.getResponseCode();
                    //System.out.println(respCode);
                }
                catch(ConnectionNotFoundException ex){
                    today = midlet.up.today;
                    midlet.connectionIsAvailable = false;
                    midlet.hisPendFlag = false;
                    midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                    midlet.dialog("Error", "connection error!", trans,Dialog.TYPE_ERROR,midlet.errorImage,5000);
                    midlet.checkPtForm().showBack();
                    //break;
                }
                catch(IOException ex){
                    //midlet.serverConnectionTimer.cancel();
                    today = midlet.up.today;
                    midlet.connectionIsAvailable = false;
                    midlet.hisPendFlag = false;
                    midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                    midlet.dialog("Error", "connection error!", trans,Dialog.TYPE_ERROR,midlet.errorImage,5000);
                    midlet.checkPtForm().showBack();
                    //break;
                }
                System.out.println(respCode);
                if (respCode == HttpConnection.HTTP_OK){
                    midlet.connectionIsAvailable = true;
                    is = conn.openInputStream();
                    /*int ch ;
                    StringBuffer sb = new StringBuffer();
                    while((ch = is.read())!=-1){
                        sb.append((char)ch);
                    }
                    System.out.println(sb.toString());*/
                        
                    viewXML(is);
               
                    //System.out.println(midlet.up.guardHashtable.toString());
                    //save updates
                    if(upSignature!= null){
                        RecordStore.deleteRecordStore("guardRecords");//remove previous records
                        byte [] byts = (upSignature+"*"+midlet.up.guardHashtable.toString()+"*"+midlet.up.locationsHashTable.toString()+"*"+midlet.up.itemsHashtable.toString()).getBytes();//get byte representation
                        RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
                        rst.addRecord(byts, 0, byts.length);
                        rst.closeRecordStore();//save update guard records
                    }
                    //get date from phone system
                    if(today==null||today.equals("null")){
                        today = midlet.up.today;
                    }
                    else{
                        today = AskariTour.split(currentDateTime, " ").elementAt(0).toString();
                    }
                    //check whether transaction was successful
                    if(midlet.state == midlet.checkPtForm){
                        if(status.equalsIgnoreCase("SUCCESSFUL")){
                            midlet.hisPendFlag = false;
                            midlet.connectionIsAvailable = true;
                            midlet.dialog(status, status, trans,Dialog.TYPE_INFO,midlet.successImage,6000);
                            midlet.checkPtForm().showBack();
                            //midlet.createTourSuccessForm(today, confNum, chkPtID,actn, descrip,status);
                            //break;
                        }
                        else{
                            midlet.connectionIsAvailable = true;
                            //Dialog.show("Error", new Label(status), null, Dialog.TYPE_INFO, null, 5000,getTransition());
                            /*if(status.length()>18){
                                status = status.substring(0, 18);
                            }*/
                            midlet.dialog("Error", status, trans,Dialog.TYPE_ERROR,midlet.errorImage,6000);
                            midlet.checkPtForm().showBack();
                            //break;
                        }
                    }
                }
                else{
                    midlet.connectionIsAvailable = false;
                    midlet.hisPendFlag = false;
                    today = midlet.up.today;
                    status = "";
                    //exceptionOccured = true;
                    midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                    midlet.dialog("Error", "connection error!", trans,Dialog.TYPE_ERROR,midlet.errorImage,5000);
                    midlet.checkPtForm().showBack();
                    //break;
                }
            }
            catch(ParseException ex){
                //break;
            }
            catch (ConnectionNotFoundException ex) {
                //ex.printStackTrace();
                //midlet.serverConnectionTimer.cancel();
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                midlet.dialog("Error", "connection error!", trans,Dialog.TYPE_ERROR,midlet.errorImage,5000);
                midlet.checkPtForm().showBack();
                //break;
            }
            catch (IOException ex) {
                //ex.printStackTrace();
                //midlet.serverConnectionTimer.cancel();
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                midlet.activeTrxn.delete(0, midlet.activeTrxn.length());
                midlet.dialog("Error", "connection error!", trans,Dialog.TYPE_ERROR,midlet.errorImage,5000);
                midlet.checkPtForm().showBack();
                //break;
            }
            catch (Exception ex) {
                //ex.printStackTrace();
                //break;
            }
            finally{
                if(is!=null){
                    try {
                        is.close();
                    } 
                    catch (IOException ex) {
                    }
                }
                if(os!=null){
                    try {
                        os.close();
                    } 
                    catch (IOException ex) {
                    }
                }
                if(conn!=null){
                    try {
                        conn.close();
                    } 
                    catch (IOException ex) {
                    }
                }
            }
        //}
    }
    
    public void viewXML(InputStream is){
        ParseEvent pe;
        String locaction = "";
        String action= new String();
        String idt = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),deploymentOfficerName = new String(),
                                deploymentOfficerID = new String(),deploymentOfficerMobileNumber= new String(),
                                officerDets,itemID = new String(),itemName = new String(),itemUnit = new String();
        
        try{
        InputStreamReader xmlReader = new InputStreamReader(is);
        XmlParser parser = new XmlParser( xmlReader );
        parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
        parser.read(Xml.START_TAG, null, replyRootTag);
        boolean trucking = true;
        while (trucking) {
            pe = parser.read();
            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                pe = parser.read();
                txNum= pe.getText();
            }
            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                pe = parser.read();
                confNum = pe.getText();
            }
            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                pe = parser.read();
                status= pe.getText();
            }
            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("datetime")) {
                pe = parser.read();
                currentDateTime= pe.getText();
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                pe = parser.read();
                upSignature = pe.getText();
            }
            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                String nemu = pe.getName();
                if (nemu.equals("update")) {
                    while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                        pe = parser.read();
                        if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                            String name1 = pe.getName();
                            if (name1.equals("record")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                        pe = parser.read();
                                        gName = pe.getText();
                                        name = gName;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                        pe = parser.read();
                                        number = pe.getText();
                                        phoneNum = number;
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                        pe = parser.read();
                                        idt = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                       pe = parser.read();
                                        action = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                        pe = parser.read();
                                        locaction = pe.getText();
                                    }
                                }
                                //hashtable for guards
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(number==null||locaction==null){
                                        number = "null";
                                        locaction = "null";
                                    }
                                    midlet.up.guardHashtable.put(idt.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    midlet.up.guardHashtable.remove(idt.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                    pe = parser.read();
                            }
                        }
                        else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("deployment")){
                            String nam = pe.getName();
                            if (nam.equals("deployment")) {
                                while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentName")) {
                                        pe = parser.read();
                                        deploymentName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerName")) {
                                        pe = parser.read();
                                        deploymentOfficerName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerMobileNumber")) {
                                        pe = parser.read();
                                        deploymentOfficerMobileNumber = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentOfficerID")) {
                                        pe = parser.read();
                                        deploymentOfficerID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("deploymentID")) {
                                        pe = parser.read();
                                        deploymentID = pe.getText();
                                    }
                                }
                                //hashtable for locations
                                officerDets = deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+deploymentName.trim();
                                if("INSERT".equalsIgnoreCase(action)){
                                    midlet.up.locationsHashTable.put(deploymentID.trim(), officerDets);
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    midlet.up.locationsHashTable.remove(deploymentID.trim());
                                }
                            }
                            else {
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                    pe = parser.read();
                            }
                        }
                        else if (pe.getType() == Xml.START_TAG&&pe.getName().equals("item")){
                            String atbNemu = pe.getName();
                            if (atbNemu.equals("item")) {
                                while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(atbNemu) == false)){
                                    pe = parser.read();
                                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemID")) {
                                        pe = parser.read();
                                        itemID = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemName")) {
                                        pe = parser.read();
                                        itemName = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("itemUnit")) {
                                        pe = parser.read();
                                        itemUnit = pe.getText();
                                    }
                                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                        pe = parser.read();
                                        action = pe.getText();
                                    }
                                }
                                //hashtable for items
                                if("INSERT".equalsIgnoreCase(action)){
                                    if(itemID==null||itemName==null||itemUnit==null){
                                        itemUnit = "null";
                                        itemName = "null";
                                        itemID = "null";
                                    }
                                    midlet.up.unitsVector.addElement(itemUnit);//avoid duplicates
                                    midlet.up.itemsHashtable.put(itemID.trim(), itemName.trim()+"_"+itemUnit.trim());
                                }
                                else if("DELETE".equalsIgnoreCase(action)){
                                    midlet.up.itemsHashtable.remove(itemID.trim());
                                }
                            }
                            else{
                                while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(atbNemu) == false))
                                    pe = parser.read();
                            }
                        }
                    }
                }
                else {
                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                    pe = parser.read();
                }
            }
            if (pe.getType() == Xml.END_TAG &&pe.getName().equals(replyRootTag))
                trucking = false;
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    private String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum){
        //time = midlet.up.today;
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        if(name.equals("Guard")){
            name = "";
            xmlStr.append("<name>").append(name).append("</name>");
        }
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<deploymentID>").append(locationID).append("</deploymentID>");
        xmlStr.append("<deploymentName>").append(location).append("</deploymentName>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</record>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    private String createTransactionXML(String location,int expShifts,String pin,String txNum,String locationID,String adj){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<supervRecord>");//locationID
        xmlStr.append("<deploymentName>").append(location).append("</deploymentName>");
        xmlStr.append("<deploymentID>").append(locationID).append("</deploymentID>");
        xmlStr.append("<expShifts>").append(expShifts).append("</expShifts>");//
        xmlStr.append("<adj>").append(adj).append("</adj>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</supervRecord>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    private String createTransactionXML(String opt,String chkptID,String imei,String pin,String txNum){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<tourRecord>");//locationID
        xmlStr.append("<chkptID>").append(chkptID).append("</chkptID>");
        xmlStr.append("<otp>").append(opt).append("</otp>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<imei>").append(midlet.imei).append("</imei>");//
        xmlStr.append("<iccid>").append(midlet.iccid).append("</iccid>");//
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</tourRecord>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    private String createTransactionXML(String location,String locationID,String itemID,String itemName,String itemUnit,String itemQty, String pin,String transNum){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<stkRecord>");//locationID
        xmlStr.append("<deploymentName>").append(location).append("</deploymentName>");
        xmlStr.append("<deploymentID>").append(locationID).append("</deploymentID>");
        xmlStr.append("<itemID>").append(itemID).append("</itemID>");//
        xmlStr.append("<itemName>").append(itemName).append("</itemName>");
        //xmlStr.append("<itemUnit>").append(itemUnit).append("</itemUnit>");
        xmlStr.append("<itemQuantity>").append(itemQty).append("</itemQuantity>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<transNo>").append(transNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</stkRecord>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    public Transition getTransition(){
        if(trans == null){
            trans = CommonTransitions.createFade(1000);
        }
        return trans;
    }
}
